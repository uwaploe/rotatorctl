package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"math"
	"os"
	"time"

	"bitbucket.org/uwaploe/go-covis/pkg/translator"
	"github.com/pkg/errors"
)

// Angle represents a rotation of the platform
type Angle struct {
	Roll    float64 `json:"roll"`
	Pitch   float64 `json:"pitch"`
	Heading float64 `json:"heading"`
}

// ToPb converts an Angle to a Protocol Buffer message format
func (a *Angle) ToPb() *translator.Orientation {
	return &translator.Orientation{
		Roll:    int32(a.Roll * translator.ANGLESCALE),
		Pitch:   int32(a.Pitch * translator.ANGLESCALE),
		Heading: int32(a.Heading * translator.ANGLESCALE),
	}
}

// Convert a rotation array to a Protocol Buffer Rotation message
func RotationPb(r [3]float64) *translator.Rotation {
	return &translator.Rotation{
		Roll: int32(r[0] * translator.ANGLESCALE),
		Tilt: int32(r[1] * translator.ANGLESCALE),
		Pan:  int32(r[2] * translator.ANGLESCALE),
	}
}

// State contains the current orientation state of the platform
type State struct {
	T time.Time `json:"t"`
	// Angle of each Rotator in their own coordinate system
	Rotation [3]float64 `json:"rotation"`
	Stalled  uint       `json:"stall_flag"`
	Angle
}

func (s *State) String() string {
	b, err := json.MarshalIndent(s, "", "  ")
	if err != nil {
		return ""
	}
	return string(b)
}

// StateFromPb creates a State struct from a Protocol Buffer message
func StateFromPb(msg *translator.State) *State {
	if msg == nil {
		return nil
	}
	s := State{}
	s.T = time.Now().UTC()
	if msg.R != nil {
		s.Rotation[0] = float64(msg.R.Roll) / translator.ANGLESCALE
		s.Rotation[1] = float64(msg.R.Tilt) / translator.ANGLESCALE
		s.Rotation[2] = float64(msg.R.Pan) / translator.ANGLESCALE
		s.Stalled = uint(msg.R.Stalled)
	}
	if msg.O != nil {
		s.Roll = float64(msg.O.Roll) / translator.ANGLESCALE
		s.Pitch = float64(msg.O.Pitch) / translator.ANGLESCALE
		s.Heading = float64(msg.O.Heading) / translator.ANGLESCALE
	}

	return &s
}

type stateFunc func() (*translator.State, error)

// Monitor translator motion and return the final state
func motionMonitor(ctx context.Context, next stateFunc,
	w io.Writer) (*translator.State, error) {
	var (
		state *translator.State
		err   error
	)

	for {
		state, err = next()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}
		select {
		case <-ctx.Done():
			w.Write([]byte("Interrupted\n"))
			return state, ctx.Err()
		default:
		}
		if state != nil {
			fmt.Fprintf(w, "%s\n", StateFromPb(state))
		}
	}

	return state, nil
}

func getstate(ctx context.Context,
	client translator.TranslatorClient) (*translator.State, error) {
	state, err := client.GetState(ctx, &translator.Empty{})
	if err != nil {
		return nil, errors.Wrap(err, "get state")
	}
	return state, err
}

func move(ctx context.Context, client translator.TranslatorClient,
	a *Angle) (*translator.State, error) {
	o := a.ToPb()
	stream, err := client.Move(ctx, o)
	if err != nil {
		return nil, errors.Wrap(err, "start move")
	}
	_, err = motionMonitor(ctx, stream.Recv, os.Stdout)
	if err != nil {
		return nil, errors.Wrap(err, "move")
	}

	return getstate(ctx, client)
}

func moveto(ctx context.Context, client translator.TranslatorClient,
	rangles [3]float64) (*translator.State, error) {

	r := RotationPb(rangles)
	stream, err := client.Goto(ctx, r)
	if err != nil {
		return nil, errors.Wrap(err, "start goto")
	}
	_, err = motionMonitor(ctx, stream.Recv, os.Stdout)
	if err != nil {
		return nil, errors.Wrap(err, "goto")
	}

	return getstate(ctx, client)
}

type Commander interface {
	Help() string
	Synopsis() string
	Validate(args []string) error
	Run(ctx context.Context, args []string) (string, error)
}

type levelCommand struct {
	client translator.TranslatorClient
}

func (c *levelCommand) Synopsis() string {
	return "level the COVIS platform, optionally specify a heading"
}

func (c *levelCommand) Help() string {
	return "usage: level [--heading=DEG]"
}

func (c *levelCommand) Validate(args []string) error {
	return nil
}

func avgHeading(ctx context.Context, client translator.TranslatorClient,
	n int) (float64, error) {
	val := float64(0)
	for i := 0; i < n; i++ {
		state, err := getstate(ctx, client)
		if err != nil {
			return 0, err
		}
		s := StateFromPb(state)
		val += s.Heading
	}
	return val / float64(n), nil
}

func (c *levelCommand) Run(ctx context.Context, args []string) (string, error) {
	var heading float64
	fs := flag.NewFlagSet("level", flag.ContinueOnError)
	fs.Float64Var(&heading, "heading", -1., "set magnetic heading")
	err := fs.Parse(args)
	if err != nil {
		return "", err
	}

	empty := &translator.Empty{}
	stream, err := c.client.Level(ctx, empty)
	trstate, err := motionMonitor(ctx, stream.Recv, os.Stdout)
	state := StateFromPb(trstate)
	if err == nil && heading >= 0.0 {
		hdg, err := avgHeading(ctx, c.client, 20)
		if err != nil {
			return "", err
		}
		dh := heading - hdg
		trstate, err = move(ctx, c.client, &Angle{Heading: dh})
		if err != nil {
			// Try to rotate the other way
			trstate, err = move(ctx, c.client, &Angle{Heading: math.Remainder(dh, 360.0)})
			if err != nil {
				return "", err
			}
		}
		state = StateFromPb(trstate)
	}
	return state.String(), err
}

type moveCommand struct {
	client translator.TranslatorClient
}

func (c *moveCommand) Synopsis() string {
	return "make a relative adjustment to the COVIS platform"
}

func (c *moveCommand) Help() string {
	return "usage: move [--pitch=DEG] [--roll=DEG] [--heading=DEG]"
}

func (c *moveCommand) Validate(args []string) error {
	return nil
}

func (c *moveCommand) Run(ctx context.Context, args []string) (string, error) {
	var heading, pitch, roll float64
	fs := flag.NewFlagSet("move", flag.ContinueOnError)
	fs.Float64Var(&heading, "heading", 0., "relative heading change")
	fs.Float64Var(&pitch, "pitch", 0., "relative pitch change")
	fs.Float64Var(&roll, "roll", 0., "relative roll change")
	err := fs.Parse(args)
	if err != nil {
		return "", err
	}

	trstate, err := move(ctx, c.client,
		&Angle{
			Roll:  roll,
			Pitch: pitch})
	if err != nil {
		return "", err
	}

	if heading != 0 {
		trstate, err = move(ctx, c.client,
			&Angle{
				Heading: heading})
		if err != nil {
			trstate, err = move(ctx, c.client,
				&Angle{
					Heading: math.Remainder(heading, 360.0)})
			if err != nil {
				return "", err
			}
		}
	}

	return StateFromPb(trstate).String(), nil
}

type readCommand struct {
	client translator.TranslatorClient
}

func (c *readCommand) Synopsis() string {
	return "return the current state of the COVIS platform"
}

func (c *readCommand) Help() string {
	return "usage: read"
}

func (c *readCommand) Validate(args []string) error {
	return nil
}

func (c *readCommand) Run(ctx context.Context, args []string) (string, error) {
	trstate, err := c.client.GetState(ctx, &translator.Empty{})
	if err != nil {
		return "", err
	}
	return StateFromPb(trstate).String(), nil
}

type gotoCommand struct {
	client translator.TranslatorClient
}

func (c *gotoCommand) Synopsis() string {
	return "make a absolute rotator adjustment to the COVIS platform"
}

func (c *gotoCommand) Help() string {
	return "usage: goto [--tilt=DEG] [--roll=DEG] [--pan=DEG]"
}

func (c *gotoCommand) Validate(args []string) error {
	return nil
}

func (c *gotoCommand) Run(ctx context.Context, args []string) (string, error) {
	var pan, tilt, roll float64
	fs := flag.NewFlagSet("goto", flag.ContinueOnError)
	fs.Float64Var(&pan, "pan", -1., "pan rotator angle")
	fs.Float64Var(&tilt, "tilt", -1., "tilt rotator angle")
	fs.Float64Var(&roll, "roll", -1., "roll rotator angle")
	err := fs.Parse(args)
	if err != nil {
		return "", err
	}

	var angles [3]float64
	angles[translator.ROLL] = roll
	angles[translator.TILT] = tilt
	angles[translator.PAN] = pan

	trstate, err := moveto(ctx, c.client, angles)
	if err != nil {
		return "", err
	}

	return StateFromPb(trstate).String(), nil
}

type modeCommand struct {
	client translator.TranslatorClient
}

func (c *modeCommand) Synopsis() string {
	return "set the rotator mode"
}

func (c *modeCommand) Help() string {
	return "usage: mode drive|seek"
}

func (c *modeCommand) Validate(args []string) error {
	if len(args) != 1 {
		return fmt.Errorf("Missing argument")
	}

	switch args[0] {
	case "drive", "seek":
		return nil
	}

	return fmt.Errorf("Invalid mode")
}

func (c *modeCommand) Run(ctx context.Context, args []string) (string, error) {
	mode := translator.Mode{}
	switch args[0] {
	case "drive":
		mode.UseSeek = false
	case "seek":
		mode.UseSeek = true
	}

	_, err := c.client.SetMode(ctx, &mode)
	return "", err
}
