module bitbucket.org/uwaploe/rotatorctl

require (
	bitbucket.org/uwaploe/go-covis v1.21.0
	github.com/c-bata/go-prompt v0.2.3
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/mattn/go-runewidth v0.0.4 // indirect
	github.com/mattn/go-shellwords v1.0.6
	github.com/mattn/go-tty v0.0.0-20190424173100-523744f04859 // indirect
	github.com/pkg/errors v0.8.1
	github.com/pkg/term v0.0.0-20190109203006-aa71e9d9e942 // indirect
	google.golang.org/grpc v1.23.1
)
