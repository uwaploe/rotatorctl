// Rotatorctl provides a simple command interpreter to interact
// with the COVIS rotator gRPC server.
package main

import (
	"context"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"strings"
	"syscall"

	"bitbucket.org/uwaploe/go-covis/pkg/translator"
	prompt "github.com/c-bata/go-prompt"
	shellwords "github.com/mattn/go-shellwords"
	"google.golang.org/grpc"
)

var Version = "dev"
var BuildDate = "unknown"

var usage = `
Usage: rotatorctl [options] server

Interactive interface to the COVIS rotator gRPC server
`

var (
	showvers = flag.Bool("version", false,
		"Show program version information and exit")
)

type completer struct {
	cmds map[string]Commander
}

type executor struct {
	cmds map[string]Commander
}

func (c *completer) complete(d prompt.Document) []prompt.Suggest {
	bc := d.TextBeforeCursor()
	if bc == "" {
		return nil
	}

	args := strings.Split(bc, " ")
	var s []prompt.Suggest
	switch args[0] {
	case "level":
		s = []prompt.Suggest{
			{Text: "--heading="},
		}
	case "move":
		s = []prompt.Suggest{
			{Text: "--heading="},
			{Text: "--pitch="},
			{Text: "--roll="},
		}
	case "goto":
		s = []prompt.Suggest{
			{Text: "--pan="},
			{Text: "--tilt="},
			{Text: "--roll="},
		}
	case "read":
	case "mode":
		s = []prompt.Suggest{
			{Text: "drive"},
			{Text: "seek"},
		}
	default:
		if len(args) == 1 {
			// number of commands + help
			cmdNames := make([]prompt.Suggest, len(c.cmds)+1)
			cmdNames = append(cmdNames,
				prompt.Suggest{Text: "help", Description: "show help message"})
			for name, cmd := range c.cmds {
				cmdNames = append(cmdNames,
					prompt.Suggest{Text: name, Description: cmd.Synopsis()})
			}

			s = cmdNames
		}
	}

	return prompt.FilterHasPrefix(s, d.GetWordBeforeCursor(), true)
}

func (e *executor) execute(l string) {
	if l == "quit" || l == "exit" {
		os.Exit(0)
		return
	}

	// Ignore spaces
	if len(strings.TrimSpace(l)) == 0 {
		return
	}

	parts, err := shellwords.Parse(l)
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v\n", err)
		return
	}

	if parts[0] == "help" {
		showHelp(os.Stdout, e.cmds)
		return
	}

	cmd, ok := e.cmds[parts[0]]
	if !ok {
		fmt.Fprintf(os.Stderr, "Unknown command: %q\n", parts[0])
		return
	}

	var args []string
	if len(parts) != 1 {
		args = parts[1:]
	}

	if err := cmd.Validate(args); err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v\n", err)
		return
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	// Catch signals
	go func() {
		s, more := <-sigs
		if more {
			switch s {
			case syscall.SIGINT:
				cancel()
			case syscall.SIGTERM, syscall.SIGHUP:
				os.Exit(1)
			}
		}
	}()

	result, err := cmd.Run(ctx, args)
	if err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v\n", err)
	} else {
		fmt.Fprintln(os.Stdout, result)
	}
}

func showHelp(w io.Writer, cmds map[string]Commander) {
	var maxLen int
	// slice of [name, synopsis]
	text := make([][]string, 0, len(cmds))
	for name, cmd := range cmds {
		text = append(text, []string{name, cmd.Synopsis()})
		if len(name) > maxLen {
			maxLen = len(name)
		}
	}

	text = append(text, []string{"quit", "exit program"})
	if maxLen < 4 {
		maxLen = 4
	}

	var cmdText string
	for name, cmd := range cmds {
		cmdText += fmt.Sprintf("  %-"+strconv.Itoa(maxLen)+"s    %s\n",
			name, cmd.Synopsis())
	}

	fmt.Fprintf(w, "Available commands:\n%s", cmdText)
}

// Start a gRPC client to control the Translator subsystem.
func grpcClient(address string) (*grpc.ClientConn, error) {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())

	conn, err := grpc.Dial(address, opts...)
	if err != nil {
		return nil, err
	}

	return conn, err
}

func setupRepl(address string) *prompt.Prompt {
	conn, err := grpcClient(address)
	if err != nil {
		log.Fatal(err)
	}
	client := translator.NewTranslatorClient(conn)

	cmds := map[string]Commander{
		"level": &levelCommand{client: client},
		"move":  &moveCommand{client: client},
		"read":  &readCommand{client: client},
		"goto":  &gotoCommand{client: client},
		"mode":  &modeCommand{client: client},
	}

	executor := &executor{cmds: cmds}
	completer := &completer{cmds: cmds}

	return prompt.New(
		executor.execute,
		completer.complete,
		prompt.OptionPrefix(fmt.Sprintf("%s> ", address)),
		prompt.OptionTitle("COVIS Rotator Control"))
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()
	if len(args) < 1 {
		fmt.Fprintln(os.Stderr, "Missing server (HOST:PORT) argument")
		flag.Usage()
		os.Exit(1)
	}

	repl := setupRepl(args[0])
	repl.Run()
}
